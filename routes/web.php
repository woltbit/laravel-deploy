<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PhoneController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UploadHistoryController;
use App\Http\Controllers\PhoneBatchController;
use App\Http\Controllers\BrandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('/', function () {
    return redirect()->route('phones.index');
});

// Route::get('/dashboard', [DashboardController::class,'dashboard'])->name('dashboard');
Route::get('/batch/{batchid}', [DashboardController::class,'batch'])->name('batch');
// User
Route::get('/change-password/{success?}', [UserController::class, 'formchangepassword'])->name('formchangepassword');
Route::post('/change-password', [UserController::class, 'changepassword'])->name('changepassword');
Route::get('/users', [UserController::class, 'index'])->name('listusers');
Route::get('/user/add', [UserController::class, 'formadd'])->name('formadminuseradd');
Route::post('/user/add', [UserController::class, 'save'])->name('saveadminuser');
Route::get('/user/edit/{user}', [UserController::class, 'formedit'])->name('formadminuseredit');
Route::post('/user/{user}', [UserController::class, 'update'])->name('updateadminuser');
Route::post('/user/reset-password/{user}', [UserController::class, 'resetpassword'])->name('resetpassword');

//Brand
Route::get('/brands/list', [BrandController::class, 'index'])->name('brand.index');
Route::get('/brand/add', [BrandController::class, 'formadd'])->name('brand.formadd');
Route::post('/brand/add', [BrandController::class, 'save'])->name('brand.save');
Route::get('/brand/edit/{brand}', [BrandController::class, 'formedit'])->name('brand.formedit');
Route::post('/brand/{brand}', [BrandController::class, 'update'])->name('brand.update');
Route::get('/brand/delete/{brand}', [BrandController::class, 'delete'])->name('brand.delete');
Route::post('/brand/reset/db', [BrandController::class, 'reset'])->name('brand.resetDB');

// Upload
Route::get('/upload/phones', [UploadHistoryController::class, 'upload'])->name('phones.upload');
Route::post('/upload/phones', [UploadHistoryController::class, 'uploadsave'])->name('phones.uploadsave');

// Generate
Route::get('/phones/batch/{brand?}', [PhoneBatchController::class, 'index'])->name('phones.index');
Route::get('/phones/generatebatch/{brand}', [PhoneBatchController::class, 'batch'])->name('phones.batch');
Route::post('/phones/generatebatch', [PhoneBatchController::class, 'generate'])->name('phones.generate');
Route::get('phones/batch/detail/{batchid}', [PhoneBatchController::class, 'batchdetail'])->name('batch.detail');
Route::post('/phones/banned', [PhoneBatchController::class, 'banned'])->name('phones.banned');
Route::post('/phones/unbanned', [PhoneBatchController::class, 'unbanned'])->name('phones.unbanned');

// Phonenumbers List
Route::get('/phones/list/{brand?}', [PhoneController::class, 'list'])->name('phones.list');
