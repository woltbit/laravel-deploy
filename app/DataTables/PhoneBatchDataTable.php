<?php

namespace App\DataTables;

use App\Models\PhoneBatch;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;


class PhoneBatchDataTable extends DataTable
{
    protected $brand;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('file', function($data) {
                $files = explode(",",$data->file);
                $html = "";
                foreach($files as $f) {
                    $html .= "<a class='text-blue mr-3' href='/zip/$f'>$f</a>";
                }
                return $html;
                // return "<a class='text-blue' href='/zip/$data->file'>" . $data->file . "</a>";
            })
            ->editColumn('filevcf', function($data) {
                $files = explode(",",$data->filevcf);
                $html = "";
                foreach($files as $f) {
                    $html .= "<a class='text-blue mr-3' href='/zip/$f'>$f</a>";
                }
                return $html;
                // return "<a class='text-blue' href='/zip/$data->filevcf'>" . $data->filevcf . "</a>";
            })
            ->addColumn('action', function($data) {
                $html = "<div class='flex' style='justify-content:space-evenly'>";
                if($data->status == "Success") {
                    $html .= "<div><a title='Detail' class='btn btn-primary btn-sm' href=".route('batch.detail',["batchid"=>$data->id])." >Detail</a></div>";
                }
                $html .= "</div>";
                return $html;
            })
            ->rawColumns(['file','filevcf','action']);
    }

    function __construct ($brand) {
        $this->brand = $brand;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PhoneBatch $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PhoneBatch $model)
    {
        $brand = $this->brand;
        return $model->where('brand',$brand)->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('phonebatch-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->pageLength(20)
                    ->responsive(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('status'),
            Column::make('file'),
            Column::make('filevcf'),
            Column::make('banned_detail'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(120)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PhoneBatch_' . date('YmdHis');
    }
}
