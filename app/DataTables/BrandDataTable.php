<?php

namespace App\DataTables;

use App\Models\Brand;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Auth;

class BrandDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('updated_at', function($user) {
                return $user->updated_at->toDateTimeString();
            })
            ->addColumn('action', function($data) {
                $html = "<div class='flex' style='justify-content:space-evenly'>";
                $userlogin = Auth::user();
                if($userlogin->username == 'admin') {
                    $html .= "<div><a title='Edit' class='btn btn-primary btn-sm' href=".route('brand.formedit',["brand"=>$data->id])." >Edit</a></div>";
                    $html .= "<div><a title='Delete' class='btn btn-primary btn-sm' href=".route('brand.delete',["brand"=>$data->id])." >Delete</a></div>";
                }
                $html .= "<div><a title='Reset DB' class='btn btn-danger btn-sm' onclick=\"reset('$data->id')\" >Reset DB</a></div>";
                $html .= "</div>";
                return $html;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Brand $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Brand $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('brand-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->pageLength(20)
                    ->responsive(true)
                    ->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('updated_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(240)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Brand_' . date('YmdHis');
    }
}
