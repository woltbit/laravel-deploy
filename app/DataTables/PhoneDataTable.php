<?php

namespace App\DataTables;

use App\Models\Phone;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PhoneDataTable extends DataTable
{
    protected $brand;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', function($data) {
                if($data->status == 'Banned') {
                    return "<span class='text-red'>" . $data->status . "</span>";
                }else {
                    return "<span class='text-green'>" . $data->status . "</span>";
                }
            })
            ->rawColumns(['status']);;
            
    }

    function __construct ($brand) {
        $this->brand = $brand;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Phone $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Phone $model)
    {
        $brand = $this->brand;
        return $model->where('brand',$brand)->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('phone-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->pageLength(20)
                    ->responsive(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('phone'),
            Column::make('batch_name'),
            Column::make('batch_detail'),
            Column::make('status'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Phone_' . date('YmdHis');
    }
}
