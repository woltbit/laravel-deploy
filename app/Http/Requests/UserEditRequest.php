<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('user'))
            $id = $this->route('user')->id;
        else
            $id=0;
        return [
            'name' => "required|string|max:255",
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Username harus diisi.',
        ];
    }
}
