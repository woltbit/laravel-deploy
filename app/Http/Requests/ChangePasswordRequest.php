<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentpassword' => 'required',
            'password' => 'required|confirmed|min:4',
        ];
    }

    public function messages()
    {
        return [
            'currentpassword.required' => 'Password sekarang harus diisi.',
            'password.required' => 'Password baru harus diisi.',
            'password.confirmed' => 'Password baru tidak sama dengan komfirmasi password baru.',
            'password.min' => 'Password baru harus minimal 4 huruf.',
        ];
    }
}
