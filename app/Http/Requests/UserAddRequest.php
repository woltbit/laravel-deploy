<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255|alpha_dash|unique:users,username',
            'name' => 'required|max:255',
            'password' => 'required|confirmed|min:4',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username harus diisi.',
            'username.max' => 'Username terlalu panjang.',
            'username.alpha_dash' => 'Username hanya huruf dan angka.',
            'username.unique' => 'Username sudah ada.',
            'name.required' => 'Nama harus diisi.',
            'name.max' => 'Nama terlalu panjang.',
            'password.required' => 'Password harus diisi.',
            'password.confirmed' => 'Password tidak sama dengan konfirmasi password.',
            'password.min' => 'Password harus minimal 4 huruf.',
        ];
    }
}
