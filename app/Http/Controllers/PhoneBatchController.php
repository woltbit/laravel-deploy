<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phone;
use App\Models\Brand;
use App\Models\uploadHistory;
use App\Models\PhoneBatch;
use ZipArchive;
use App\DataTables\PhoneBatchDataTable;
use Illuminate\Support\Facades\Auth;

class PhoneBatchController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }

    public function index($brand=null) {
        $page="generatebatch";
        $userbrands = Auth::user()->brands()->get();
        if($userbrands->count() == 0) {
            $brand = 0;
        } else {
            if($brand == null) {
                $brand = $userbrands->first()->id;
            }
        }
        // check brand
        $obrand = Brand::find($brand);
        if($obrand != null) {
            if (!$userbrands->contains($obrand->id)) {
                return redirect()->route('phones.index');
            }
        }
        $dataTable = new PhoneBatchDataTable($brand);
        return $dataTable->render('phone.index',compact('page','userbrands','obrand'));
    }

    public function batch($brand) {
        $page="generatebatch";
        $userbrands = Auth::user()->brands()->get();
        if($userbrands->count() == 0) {
            $brand = 0;
        } else {
            if($brand == null) {
                $brand = $userbrands->first()->id;
            }
        }
        // check brand
        $obrand = Brand::find($brand);
        if($obrand != null) {
            if (!$userbrands->contains($obrand->id)) {
                return redirect()->route('phones.index');
            }
        }
        // return $brand;
        $phoneAvailable = Phone::where('batch',null)->where('brand',$brand)->count();
        return view('phone.batch', compact('page','phoneAvailable','obrand'));
    }

    public function generate(Request $request) {
        $brand = $request->input('brand');
        $userbrands = Auth::user()->brands()->get();
        if($userbrands->count() == 0) {
            $brand = 0;
        } else {
            if($brand == null) {
                $brand = 0;
            }
        }
        // check brand
        $obrand = Brand::find($brand);
        if($obrand != null) {
            if (!$userbrands->contains($obrand->id)) {
                return redirect()->route('phones.index');
            }
        }
        $name = $request->input('name');
        $check = PhoneBatch::where('name',$name)->where('brand',$brand)->first();
        if($check) {
            toast('Nama Batch sudah ada.','error');
            return redirect()->route('phones.batch',['brand'=>$brand])->withInput($request->input());
        }
        $eachno = $request->input('eachno');
        $totalfiles = $request->input('totalfiles');
        $total = $eachno * $totalfiles;
        $phoneAvailable = Phone::where('batch',null)->count();
        if($total > $phoneAvailable) {
            toast('Jumlah nomor tidak cukup.','error');
            return redirect()->route('phones.batch',['brand'=>$brand])->withInput($request->input());
        }
        $headers = array('Name', 'Given Name', 'Group Membership', 'Phone 1 - Value');
        $zipname = $obrand->name . "_" . $name . '_csv.zip';
        $zipnamevcf = $obrand->name . "_" . $name . '_vcf.zip';
        $zipnameplus = $obrand->name . "_" . $name . '_csv_plus.zip';
        $zipnamevcfplus = $obrand->name . "_" . $name . '_vcf_plus.zip';
        
        $zip = new ZipArchive;
        $zipvcf = new ZipArchive;
        $zipplus = new ZipArchive;
        $zipvcfplus = new ZipArchive;
        $zip->open('zip/'.$zipname, ZipArchive::CREATE);
        $zipvcf->open('zip/'.$zipnamevcf, ZipArchive::CREATE);
        $zipplus->open('zip/'.$zipnameplus, ZipArchive::CREATE);
        $zipvcfplus->open('zip/'.$zipnamevcfplus, ZipArchive::CREATE);
        for ($i = 1; $i <= $totalfiles; $i++) {
            $fd = fopen('php://temp/maxmemory:1048576', 'w');
            $fdplus = fopen('php://temp/maxmemory:1048576', 'w');
            fputcsv($fd, $headers);
            fputcsv($fdplus, $headers);
            $phones = Phone::where('batch',null)->where('brand',$obrand->id)->take($eachno)->get();
            $phonenumbers = [];
            $vCard = "";
            $vCardplus = "";
            foreach($phones as $p) {
                $phonenumbers[] = $p->phone;
                $csvname = $p->name;
                $no = $p->phone;
                $noplus = "";
                if ($no[0] == '+') {
                    $no = substr($no, 1);
                    $noplus = "+" . $no;
                } else {
                    $noplus = "+" . $no;
                }
                fputcsv($fd, [$csvname, $csvname, '* myContacts', $no]);
                fputcsv($fdplus, [$csvname, $csvname, '* myContacts', $noplus]);
                $vCard .= "BEGIN:VCARD\r\n";
                $vCard .= "VERSION:3.0\r\n";
                $vCard .= "FN:" . $csvname . "\r\n";
                $vCard .= "TITLE:" . $csvname . "\r\n";
                $vCard .= "TEL;TYPE=work,voice:" . $no . "\r\n"; 
                $vCard .= "END:VCARD\r\n";

                $vCardplus .= "BEGIN:VCARD\r\n";
                $vCardplus .= "VERSION:3.0\r\n";
                $vCardplus .= "FN:" . $csvname . "\r\n";
                $vCardplus .= "TITLE:" . $csvname . "\r\n";
                $vCardplus .= "TEL;TYPE=work,voice:" . $noplus . "\r\n"; 
                $vCardplus .= "END:VCARD\r\n";
            }
            $nomorfile = sprintf("%04d", $i);
            Phone::whereIn('phone',$phonenumbers)->where('brand',$obrand->id)->update(['batch'=>0, 'batch_detail'=>$obrand->name . "_" . $name.'-'.$nomorfile.'.csv']);
            rewind($fd);
            rewind($fdplus);
            $zip->addFromString($obrand->name . "_" . $name.'-'.$nomorfile.'.csv', stream_get_contents($fd) );
            $zipvcf->addFromString($obrand->name . "_" . $name.'-'.$nomorfile.'.vcf', $vCard);
            $zipplus->addFromString($obrand->name . "_" . $name.'_plus-'.$nomorfile.'.csv', stream_get_contents($fdplus) );
            $zipvcfplus->addFromString($obrand->name . "_" . $name.'_plus-'.$nomorfile.'.vcf', $vCardplus);
            
            fclose($fd);
            fclose($fdplus);

        }
        $zip->close();
        $zipvcf->close();
        // update phones
        $phonebatch = new PhoneBatch();
        $phonebatch->name = $name;
        $phonebatch->file = $zipname . "," . $zipnameplus;
        $phonebatch->filevcf = $zipnamevcf . "," . $zipnamevcfplus;
        $phonebatch->totalfiles = $totalfiles;
        $phonebatch->eachno = $eachno;
        $phonebatch->status = 'Success';
        $phonebatch->brand = $obrand->id;
        $phonebatch->save();
        Phone::where('batch',0)->where('brand',$obrand->id)->update(['batch' => $phonebatch->id, 'batch_name' => $name]);
        toast('File berhasil digenerate.','success');
        return redirect()->route('phones.index',['brand'=>$obrand->id]);
    }

    public function batchdetail($batchid) {
        $page="generatebatch";
        $batch = PhoneBatch::find($batchid);
        $brand = $batch->brand;
        $userbrands = Auth::user()->brands()->get();
        if($userbrands->count() == 0) {
            $brand = 0;
        } else {
            if($brand == null) {
                $brand = 0;
            }
        }
        // check brand
        $obrand = Brand::find($brand);
        if($obrand != null) {
            if (!$userbrands->contains($obrand->id)) {
                return redirect()->route('phones.index');
            }
        }
        $current_banned = $batch->banned_detail;
        if($current_banned == null || $current_banned =='') {
            $array_current_banned = [];
        } else {
            $array_current_banned = explode(",",$current_banned);
        }
        return view('batch.detail', compact('page','batch','array_current_banned','obrand'));

    }

    public function banned(Request $request) {
        $batchid = $request->input('batchid');
        $detailid = $request->input('detailid');

        $batch = PhoneBatch::find($batchid);
        $nomorfile = sprintf("%04d", $detailid);
        $batch_detail = $batch->name . "-" . $nomorfile . ".csv";
        Phone::where("batch_detail",$batch_detail)->update(["status"=>"Banned"]);
        $current_banned = $batch->banned_detail;
        if($current_banned == null || $current_banned =='') {
            $array_current_banned = [];
        } else {
            $array_current_banned = explode(",",$current_banned);
        }
        array_push($array_current_banned, $detailid);
        $batch->banned_detail = implode(",",$array_current_banned);
        $batch->save();
        $result = [
            "message" => "Success",
        ];
        return response()->json($result);
    }

    public function unbanned(Request $request) {
        $batchid = $request->input('batchid');
        $detailid = $request->input('detailid');

        $batch = PhoneBatch::find($batchid);
        $nomorfile = sprintf("%04d", $detailid);
        $batch_detail = $batch->name . "-" . $nomorfile . ".csv";
        Phone::where("batch_detail",$batch_detail)->update(["status"=>"Clean"]);
        $current_banned = $batch->banned_detail;
        if($current_banned == null || $current_banned =='') {
            $array_current_banned = [];
        } else {
            $array_current_banned = explode(",",$current_banned);
        }
        $array_detail = array($detailid);
        $array_current_banned = array_diff($array_current_banned, $array_detail);
        $batch->banned_detail = implode(",",$array_current_banned);
        $batch->save();
        $result = [
            "message" => "Success",
        ];
        return response()->json($result);
    }
}
