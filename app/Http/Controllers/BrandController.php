<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DataTables\BrandDataTable;
use App\Models\Brand;
use App\Models\Phone;
use App\Models\PhoneBatch;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }

    public function index(BrandDataTable $dataTable) {
        $page="brand";
        return $dataTable->render('brand.list',compact('page'));
    }

    public function formadd() {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $page="brand";
        return view('brand.formadd',compact('page'));
    }

    public function save(Request $request) {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $name = $request->input('name');

        $brand = new Brand();
        $brand->name = $name;
        $brand->slug = Str::of($name)->slug('-');
        $brand->save();

        toast('Brand berhasil disimpan.','success');
        return redirect()->route('brand.index');
    }

    public function formedit(Brand $brand) {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $page="brand";
        return view('brand.formedit',compact('page','brand'));
    }

    public function update(Request $request, Brand $brand) {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $name = $request->input('name');

        $brand->name = $name;
        $brand->slug = Str::of($name)->slug('-');
        $brand->save();

        toast('Brand berhasil diupdate.','success');
        return redirect()->route('brand.index');
    }

    public function delete(Brand $brand) {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $brand->delete();

        toast('Brand berhasil dihapus.','success');
        return redirect()->route('brand.index');
    }

    public function reset(Request $request) {
        // Hapus semua data brand
        $brand = $request->input('brandid');
        $userbrands = Auth::user()->brands()->get();
        if($userbrands->count() == 0) {
            $brand = 0;
        } else {
            if($brand == null) {
                $brand = 0;
            }
        }
        // check brand
        $obrand = Brand::find($brand);
        if($obrand != null) {
            if (!$userbrands->contains($obrand->id)) {
                $result = [
                    "message" => "Tidak ada akses ke brand ini.",
                ];
                return response()->json($result);
            }
        }
        //Hapus Tabel phones
        Phone::where('brand',$obrand->id)->delete();
        //Ubah status PhoneBatch
        PhoneBatch::where('brand',$obrand->id)->update(['status' => "Reset"]);
        $result = [
            "message" => "Success",
        ];
        return response()->json($result);
    }
}
