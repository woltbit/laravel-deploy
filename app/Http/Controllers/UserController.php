<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Brand;
use App\DataTables\UserDataTable;
use App\Http\Requests\UserAddRequest;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\ResetUserRequest;
use App\Http\Requests\ChangePasswordRequest;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }

    public function index(UserDataTable $dataTable) {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $page="users";
        return $dataTable->render('user.list',compact('page'));
    }

    public function formadd() {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $page="users";
        $brands = Brand::get();
        return view('user.formadd', compact('page','brands'));
    }

    public function save(UserAddRequest $request) {
        $loguser = Auth::user();
        if($loguser->username != 'admin') abort(403);
        $username = $request->input('username');
        $name = $request->input('name');
        $password = $request->input('password');
        $brand = $request->input('brand');

        $user = new User();
        $user->username = $username;
        $user->name = $name;
        $user->password = Hash::make($password);
        $user->save();
        $user->brands()->attach($brand);

        toast('User berhasil disimpan.','success');
        return redirect()->route('listusers');
    }

    public function formedit(User $user) {
        $loguser = Auth::user();
        if($loguser->username != 'admin') abort(403);
        $page="users";
        $brands = Brand::get();
        return view('user.formedit', compact('page','user','brands'));
    }

    public function update(User $user, UserEditRequest $request) {
        $loguser = Auth::user();
        if($loguser->username != 'admin') abort(403);
        $name = $request->input('name');
        $brand = $request->input('brand');

        $user->name = $name;
        $user->save();
        $user->brands()->sync([]);
        $user->brands()->attach($brand);

        toast('User berhasil disimpan.','success');
        return redirect()->route('listusers');
    }

    // public function banned(User $user) {
        
    //     $message = "User has been ";

    //     if($user->isBanned()) {
    //         $this->authorize('unban', User::class);
    //         $user->unban();
    //         $message .= "activated.";
    //     } else {
    //         $this->authorize('ban', User::class);
    //         $user->ban();
    //         $message .= "banned.";
    //     }
    //     toast($message,'success');
    //     return redirect()->route('listusers');
    // }

    public function resetpassword(User $user, ResetUserRequest $request) {
        $user = Auth::user();
        if($user->username != 'admin') abort(403);
        $newPassword = $request->input('newpass');
        $user->password = Hash::make($newPassword);
        $user->save();
        return ["message" => "Password berhasil diganti."];
    }

    public function formchangepassword($success=null) {
        $page='';
        $user = Auth::user();
        return view('user.changepassword', compact('user', 'success','page'));
    }

    public function changepassword(ChangePasswordRequest $request) {
        $currentpassword = $request->input('currentpassword');
        $user = Auth::user();

        if(Hash::check($currentpassword, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            toast('Password berhasil diubah.','success');
            return redirect()->route('formchangepassword',['success'=>'success']);
        } else {
            toast('Password sekarang tidak sesuai.','error');
            return redirect()->route('formchangepassword',['success'=>'error']);
        }
    }
}
