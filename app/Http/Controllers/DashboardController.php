<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phone;
use App\Models\uploadHistory;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;

class DashboardController extends Controller
{
    public function dashboard() {
        $page = "dashboard";
        $tasks = uploadHistory::where('isdone',false)->get();
        foreach($tasks as $t) {
            if($t->batch == null) {
                $t->batch = "";
                $t->status = "Not Started";
                $t->totalJobs = 0;
                $t->progress = 0;
            } else {
                $t->status = "Processing";
                $batch = Bus::findBatch($t->batch);
                $t->totalJobs = $batch->totalJobs;
                $t->progress = $batch->totalJobs - $batch->pendingJobs;
            }
        }
        return view('dashboard', compact('page','tasks'));
    }

    public function batch($batchid) {
        $batch = Bus::findBatch($batchid);
        return $batch;
    }
}
