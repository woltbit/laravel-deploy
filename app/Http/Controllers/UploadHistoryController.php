<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\uploadHistory;
use Carbon\Carbon;
use App\Jobs\processCsv;
use App\Jobs\importCsv;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Auth;

class UploadHistoryController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }
    
    public function upload() {
        $user = Auth::user();
        $userbrands = $user->brands()->get();
        $page="phones";
        $tasks = uploadHistory::where('isdone',false)->get();
        foreach($tasks as $t) {
            if($t->batch == null) {
                $t->batch = "";
                $t->status = "Not Started";
                $t->totalJobs = 0;
                $t->progress = 0;
                $t->failedJobs = 0;
            } else {
                $batch = Bus::findBatch($t->batch);
                $finish = $batch->finished();
                if($finish) {
                    $t->isdone = true;
                    $t->save();
                    $t->status = "Finished";
                } else {
                    $t->status = "Processing";
                }
                $t->totalJobs = $batch->totalJobs;
                $t->progress = $batch->totalJobs - $batch->pendingJobs;
                $t->failedJobs = $batch->failedJobs;
            }
        }
        return view('phone.upload', compact('page','tasks','userbrands'));
    }

    public function uploadsave(Request $request) {
        $brand = $request->input('brand');
        // Check brand
        $user = Auth::user();
        $userbrands = $user->brands()->get();
        $found = false;
        foreach($userbrands as $b) {
            if($b->id == $brand) {
                $found = true;
                break;
            }
        }
        if(!$found) {
            toast('Brand tidak ditemukan.','error');
            return redirect()->route('phone.upload');
        }
        // Check file
        $file = $request->file('csvfile');
        $valid_extension = array("csv");
        $tempPath = $file->getRealPath();
        $filename = Carbon::now()->timestamp . "_" . $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        if (!in_array(strtolower($extension), $valid_extension)) {
            return redirect()->route('phones.upload')->withErrors(['csvfile'=> 'Hanya bisa menerima file csv']);
        }
        $location = 'uploads'; 
        $file->move($location, $filename);
        $batch = Bus::batch([])->dispatch();
        $filepath = public_path($location . "/" . $filename);
        $history = new uploadHistory();
        $history->filelocation = $filepath;
        $history->filename = $file->getClientOriginalName();
        $history->batch = $batch->id;
        $history->brand= $brand;
        $history->save();
        processCsv::dispatch($history->id);
        
        toast('File berhasil diupload.','success');
        return redirect()->route('phones.upload');
    }
}
