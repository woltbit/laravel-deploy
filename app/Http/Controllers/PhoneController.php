<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phone;
use App\Models\Brand;
use App\DataTables\PhoneDataTable;
use Illuminate\Support\Facades\Auth;

class PhoneController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }

    public function list($brand = null) {
        $page = "phoneslist";
        $userbrands = Auth::user()->brands()->get();
        if($userbrands->count() == 0) {
            $brand = 0;
        } else {
            if($brand == null) {
                $brand = $userbrands->first()->id;
            }
        }
        // check brand
        $obrand = Brand::find($brand);
        if($obrand != null) {
            if (!$userbrands->contains($obrand->id)) {
                return redirect()->route('phones.list');
            }
        }
        $dataTable = new PhoneDataTable($brand);

        return $dataTable->render('phone.list', compact('page','userbrands','obrand'));
    }

    
}
