<?php

namespace App\Jobs;

use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Phone;

class importCsv implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $number;
    protected $brand;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($number, $brand)
    {
        $this->number = $number;
        $this->brand = $brand;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $faker = \Faker\Factory::create();
        $name = $faker->name(); //Get the name
        $number = $this->number;
        $p = Phone::where('phone',$number)->where('brand',$this->brand)->first();
        if($p == null) {
            $phone = new Phone();
            $phone->name = $name;
            $phone->phone = $number;
            $phone->status = "Clean";
            $phone->brand = $this->brand;
            $phone->save();
        }
        
    }
}
