@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col">
        <h1 class="m-0">Daftar Batch Detail Files</h1>
        </div>
    </div>
    </div>
</div>
@endsection


@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="container">
                <div class="header">
                    <p>Batch : <strong>{{$batch->name}}</strong></p>
                </div>
                <div class="content-dt">
                    <ul class="list-disc list-inside">
                        @for ($i = 1; $i <= $batch->totalfiles; $i++)
                        @if (in_array($i,$array_current_banned))
                        <li class="mb-2"><span class="text-red">{{$obrand->name}}_{{$batch->name}}-{{sprintf("%04d", $i)}}.csv </span>
                            <span class="ml-5">
                                <a class="btn btn-sm btn-success" onclick="unbanned('{{$batch->id}}','{{$i}}')">Unbanned</a>
                            </span>
                        </li>
                        @else
                        <li class="mb-2">{{$obrand->name}}_{{$batch->name}}-{{sprintf("%04d", $i)}}.csv 
                            <span class="ml-5">
                                <a class="btn btn-sm btn-danger" onclick="banned('{{$batch->id}}','{{$i}}')">Banned</a>
                            </span>
                        </li>
                        @endif
                        @endfor
                    </ul>
                </div>
            </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function banned(batchid, detailid) {
        console.log('banned');
        var url = "{{route('phones.banned')}}";
        axios.post(url, {
            detailid: detailid,
            batchid: batchid
        }).then(function(response) {
            data = response.data;
            if(data.message === "Success") {
                setTimeout(function() {
                    window.location.reload();
                }, 100);
            } else {
                alert(data.message);
            }
        })
    }

    function unbanned(batchid, detailid) {
        console.log('unbanned');
        var url = "{{route('phones.unbanned')}}";
        axios.post(url, {
            detailid: detailid,
            batchid: batchid
        }).then(function(response) {
            data = response.data;
            if(data.message === "Success") {
                setTimeout(function() {
                    window.location.reload();
                }, 100);
            } else {
                alert(data.message);
            }
        })
    }
</script>
@endpush