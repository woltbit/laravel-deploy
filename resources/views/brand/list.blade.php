@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col">
        <h1 class="m-0">Brands</h1>
        </div>
    </div>
    </div>
</div>
@endsection


@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="content-dt">
                <div class="toolbar">
                    @if(auth()->user()->username == 'admin')
                    <a href="{{ route('brand.formadd') }}" class="btn btn-primary btn-sm">Tambah</a>
                    @endif
                </div>
                {{$dataTable->table(['class'=>'table table-sm dt-table'])}}
            </div>
    </div>
</div>
@endsection

@push('scripts')
{{$dataTable->scripts()}}
<script>
    function reset(brandid) {
        if(confirm('Reset DB?')) {
            var url = "{{ route('brand.resetDB') }}";
            axios.post(url, {
                brandid: brandid,
            }).then(function(response) {
                data = response.data;
                if(data.message == 'Success') {
                    alert("Reset DB Success");
                } else {
                    alert(data.message);
                }
            })
        }
    }
</script>
@endpush