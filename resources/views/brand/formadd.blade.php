@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Tambah Brand</h1>
        </div>
    </div>
    </div>
</div>
@endsection

@section('content')
    <div class="container mx-auto mt-5 px-8">
        <div class="mt-10">
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('brand.save') }}">
                @csrf

                <!-- Username -->
                <div>
                    <x-label for="name" :value="__('Nama Brand')" />

                    <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                </div>

                <div class="flex items-center justify-start mt-4">
                    <x-button class="ml-3">
                        {{ __('Save') }}
                    </x-button>
                </div>
            </form>
        </div>
    </div>
@stop

@push('scripts')
@endpush
