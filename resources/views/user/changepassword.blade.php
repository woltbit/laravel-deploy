@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Ganti Password</h1>
        </div>
    </div>
    </div>
</div>
@endsection

@section('content')
    <div class="container mx-auto px-8">
        <div class="mt-10">
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            @if($success == "success")
            <div class="mb-4">
                <div class="font-medium text-green-600">
                    {{ __('Password berhasil diganti.') }}
                </div>
            </div>
            @endif

            @if($success == "error")
            <div class="mb-4">
                <div class="font-medium text-red-600">
                    {{ __('Password sekarang tidak sesuai.') }}
                </div>
            </div>
            @endif

            <form method="POST" action="{{ route('changepassword') }}">
                @csrf

                <!-- Current Password -->
                <div class="mt-4">
                    <x-label for="currentpassword" :value="__('Password sekarang')" />

                    <x-input id="currentpassword" class="block mt-1 w-full" type="password" name="currentpassword" required autofocus />
                </div>

                <!-- New Password -->
                <div class="mt-4">
                    <x-label for="password" :value="__('Password baru')" />

                    <x-input id="password" class="block mt-1 w-full" type="password" name="password" required />
                </div>

                <!-- Validate New Password -->
                <div class="mt-4">
                    <x-label for="password_confirmation" :value="__('Konfirmasi password baru')" />

                    <x-input id="validatenewpassword" class="block mt-1 w-full" type="password" name="password_confirmation" required />
                </div>

                <div class="flex items-center justify-end mt-4">
                    <x-button class="ml-3">
                        {{ __('Ganti') }}
                    </x-button>
                </div>
            </form>
        </div>
    </div>
@stop

@push('scripts')
@endpush
