@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Edit User - {{ $user->name }}</h1>
        </div>
    </div>
    </div>
</div>
@endsection

@section('content')
    <div class="container mx-auto mt-5 px-8">
        <div class="mt-10">
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('updateadminuser', ['user'=>$user->id]) }}">
                @csrf

                <!-- Nama -->
                <div>
                    <x-label for="name" :value="__('Nama')" />

                    <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name', $user->name)" required />
                </div>

                <div class="mt-4"><strong>Boleh akses ke brand berikut : </strong></div>
                @foreach($brands as $brand)
                <div>
                    {{ Form::checkbox('brand[]', $brand->id, $user->brands->contains($brand->id) ? true : false, ['id'=>$brand->id,'class' => 'shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 mr-2']) }}
                    <label class="text-sm text-gray-700" for="{{$brand->id}}">{{$brand->name}}</label>
                </div>
                 @endforeach
                
                <div class="flex items-center justify-start mt-4">
                    <x-button class="ml-3">
                        {{ __('Save') }}
                    </x-button>
                </div>
            </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
function selectAll(permissionsgroup) {
    $("."+permissionsgroup).prop('checked',true);
}
</script>
@endpush
