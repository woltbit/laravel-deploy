@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">User</h1>
        </div>
    </div>
    </div>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="content-dt">
                <div class="toolbar">
                    <a href="{{ route('formadminuseradd') }}" class="btn btn-primary btn-sm">Tambah</a>
                </div>
                {{$dataTable->table(['class'=>'table-auto dt-table'])}}
            </div>
        </div>
    </div>
    
@stop

@push('scripts')
    {{$dataTable->scripts()}}
    <script>
    function resetPassword(id, name) {
        event.preventDefault();
        var url = "{{route('resetpassword', '')}}" + "/" + id;
        Swal.fire({
            title: 'Reset Password for \n' + name,
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Save',
            showLoaderOnConfirm: true,
            preConfirm: (newpassword) => {
                axios.post(url, {
                    newpass: newpassword
                }, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then((res) => {
                    if (res.status == 200) {
                        if (res.data.message) {
                            Swal.fire({
                                type: 'success',
                                text: res.data.message
                            })
                        }
                    } else {
                        throw new Error(res.statusText);
                    }
                }).catch((error) => {
                    if (error.response.status == 422) {
                        var errorObj = error.response.data;
                        var listErrors = '';
                        for (var key in errorObj.errors) {
                            listErrors += '<li>' + errorObj.errors[key] + '</li>';
                        }
                        var errorHTML = '<ul>' + listErrors + '</ul>';
                        Swal.fire({
                            type: 'error',
                            title: errorObj.message,
                            html: errorHTML
                        })
                    }
                });

            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }
    </script>
@endpush

@push('styles')
    <style>
        .content-dt {
            /* overflow: scroll; */
            overflow-x: hidden;
        }

        .content-dt .toolbar {
            position: absolute;
            z-index: 1;
        }

        @media only screen and (max-width: 600px) {
            .content-dt .toolbar {
                position: relative;
                z-index: 1;
            }
        }

        .content-dt .dt-table {
            width: 100% !important;
        }

        .dt-table tr, .dt-table tr th, .dt-table tr td {
            border: 1px solid #888;
        }

        .banned {
            background-color:#FECACA !important;
        }
    </style>
@endpush