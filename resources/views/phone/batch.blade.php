@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col">
        <h1 class="m-0">Batch Nomor</h1>
        </div>
    </div>
    </div>
</div>
@endsection


@section('content')
<div class="container mx-auto mt-5 px-8">
    <div class="mt-8">
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form method="POST" action="{{ route('phones.generate') }}">
            @csrf

            <!-- Nama -->
            <div class="mt-4">
                <x-label for="name" :value="__('Nama')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
            </div>

            <!-- total files -->
            <div class="mt-4">
                <x-label for="totalfiles" :value="__('Jumlah File')" />

                <x-input id="totalfiles" onChange="hitungtotal()" class="block mt-1 w-full" type="text" name="totalfiles" :value="old('totalfiles')" required />
            </div>

            <!-- each no -->
            <div class="mt-4">
                <x-label for="eachno" :value="__('Jumlah Nomor per file')" />

                <x-input id="eachno" onChange="hitungtotal()" class="block mt-1 w-full" type="text" name="eachno" :value="old('eachno')" required />
            </div>

            <!-- Total no -->
            <div class="mt-4">
                <x-label for="totalno">Total Nomor yang akan di generate (Jumlah Nomor tersedia : {{$phoneAvailable}} )</x-label>

                <x-input id="totalno" class="block mt-1 w-full" type="text" name="totalno" />
            </div>

            <div class="flex items-center justify-start mt-4">
                <input type="hidden" name="brand" id="brand" value="{{$obrand->id}}">
                <x-button class="ml-3">
                    {{ __('Generate') }}
                </x-button>
            </div>
        </form>
    </div>

</div>
@endsection

@push('scripts')
<script>
    function hitungtotal() {
        var eachno = $('#eachno').val();
        var totalfiles = $('#totalfiles').val();
        var total = totalfiles * eachno;
        
        $('#totalno').val(total);
    }
</script>
@endpush