@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col">
        <h1 class="m-0">Upload Nomor HP dari CSV </h1>
        </div>
    </div>
    </div>
</div>
@endsection


@section('content')
<script>
    
    function refreshStatus(batch,id){
        axios.get('/batch/'+batch).then(function(response) {
            var data = response.data;
            var processedJobs = data.processedJobs;
            var failedJobs = data.failedJobs;
            var totalJobs = data.totalJobs
            var finishedAt = data.finishedAt;
            $("#s"+id).html("(" + processedJobs + "/" + totalJobs + ")");
            if(finishedAt == null) {
                setTimeout(refreshStatus, 1000, batch, id);
                $("#st"+id).html("Processing");
            } else {
                $("#st"+id).html("Finished");
            }
        });
    }
</script>
<div class="container mx-auto mt-5 px-8">
    <div class="mt-8">
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form method="POST" enctype="multipart/form-data" action="{{ route('phones.uploadsave') }}">
            @csrf

            <div class="mt-4">
                <select name="brand" id="brand">
                    <option value="" selected disabled>-- Pilih Brand --</option>
                    @foreach($userbrands as $brand)
                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                    @endforeach
                </select>

            </div>

            <!-- Image -->
            <div class="mt-4">
                <x-label for="csvfile" :value="__('File CSV')" />

                <input type="file" id="csvfile" name="csvfile">
            </div>

            <div class="flex items-center justify-start mt-4">
                <x-button class="ml-3">
                    {{ __('Upload') }}
                </x-button>
            </div>
        </form>
    </div>

    <div class="mt-8">
        <p><strong>Task Upload</strong></p>
        @if(count($tasks) > 0)
        <table class="table table-sm ">
            <tr>
                <td>Job id</td>
                <td>File</td>
                <td>Status</td>
            </tr>
            @foreach ($tasks as $t)
            <tr>
                <td>{{ $t->batch }}</td>
                <td>{{ $t->filename }}</td>
                <td><span id="st{{$t->id}}">{{ $t->status }}</span> <span id="s{{$t->id}}">({{ $t->progress }}/{{ $t->totalJobs }})</span></td>
            </tr>
            <script>setTimeout(refreshStatus, 1000, "{{$t->batch}}", "{{$t->id}}");</script>
            @endforeach


        </table>
        @else
        <p>No tasks</p>
        @endif
    </div>
</div>
@endsection