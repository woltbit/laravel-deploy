@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col">
        <h1 class="m-0">Batch Download</h1>
        </div>
    </div>
    </div>
</div>
@endsection


@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="content-dt">
                <div class="toolbar">
                    <select onChange="refresh(this)" class="mr-2 h-10 rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                        <option value="" selected disabled>-- Pilih Brand --</option>
                        @isset($obrand)
                        @foreach($userbrands as $p)
                        <option value="{{$p->id}}" @if($p->id==$obrand->id) selected @endif)>{{ $p->name }}</option>
                        @endforeach
                        @else
                        @foreach($userbrands as $p)
                        <option value="{{$p->id}}">{{ $p->name }}</option>
                        @endforeach
                        @endisset
                    </select>
                    <a href="{{ route('phones.batch',['brand'=>$obrand->id]) }}" class="btn btn-primary btn-sm">Tambah</a>
                </div>
                {{$dataTable->table(['class'=>'table table-sm dt-table'])}}
            </div>
    </div>
</div>
@endsection

@push('scripts')
{{$dataTable->scripts()}}
<script>
function refresh(ref) {
    var url = "{{route('phones.index', '')}}"+"/"+ref.value;
    if(ref.value == "") url = "{{route('phones.index', '')}}";
    window.location.href=url;
}
</script>
@endpush