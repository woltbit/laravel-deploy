<li class="nav-item">
    @if($page=='brand')
    <a href="{{ route('brand.index') }}" class="nav-link active">
        <i class="fa-solid fa-building nav-icon"></i>
        <p>Brand</p>
    </a>
    @else
    <a href="{{ route('brand.index') }}" class="nav-link">
        <i class="fa-solid fa-building nav-icon"></i>
        <p>Brand</p>
    </a>
    @endif
</li>

@if(auth()->user()->username == 'admin')
<li class="nav-item">
    @if($page=='users')
    <a href="{{ route('listusers') }}" class="nav-link active">
        <i class="fas fa-user-tie nav-icon"></i>
        <p>User</p>
    </a>
    @else
    <a href="{{ route('listusers') }}" class="nav-link">
        <i class="fas fa-user-tie nav-icon"></i>
        <p>User</p>
    </a>
    @endif
</li>
@endif

<li class="nav-item">
    @if($page=='generatebatch')
    <a href="{{ route('phones.index') }}" class="nav-link active">
        <i class="fa-solid fa-file-csv nav-icon"></i>
        <p>Generate CSV</p>
    </a>
    @else
    <a href="{{ route('phones.index') }}" class="nav-link">
        <i class="fa-solid fa-file-csv nav-icon"></i>
        <p>Generate CSV</p>
    </a>
    @endif
</li>

<li class="nav-item">
    @if($page=='phones')
    <a href="{{ route('phones.upload') }}" class="nav-link active">
        <i class="fa-solid fa-cloud-arrow-up nav-icon"></i>
        <p>Upload Nomor</p>
    </a>
    @else
    <a href="{{ route('phones.upload') }}" class="nav-link">
        <i class="fa-solid fa-cloud-arrow-up nav-icon"></i>
        <p>Upload Nomor</p>
    </a>
    @endif
</li>

<li class="nav-item">
    @if($page=='phoneslist')
    <a href="{{ route('phones.list') }}" class="nav-link active">
        <i class="fa-solid fa-mobile nav-icon"></i>
        <p>Nomor HP</p>
    </a>
    @else
    <a href="{{ route('phones.list') }}" class="nav-link">
        <i class="fa-solid fa-mobile nav-icon"></i>
        <p>Nomor HP</p>
    </a>
    @endif
</li>