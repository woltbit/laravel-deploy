@extends('layouts.app')

@section('header')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col">
        <h1 class="m-0">Dashboard</h1>
        </div>
    </div>
    </div>
</div>
@endsection


@section('content')
<script>
    
    function refreshStatus(batch,id){
        axios.get('/batch/'+batch).then(function(response) {
            var data = response.data;
            var processedJobs = data.processedJobs;
            var totalJobs = data.totalJobs
            $("#"+id).html("(" + processedJobs + "/" + totalJobs + ")");
        });
    }
</script>
<div class="container mx-auto mt-5 px-8">
    <div class="mt-10">
        <p><strong>Task Upload</strong></p>
        @if(count($tasks) > 0)
        <table class="table table-sm ">
            <tr>
                <td>Job id</td>
                <td>File</td>
                <td>Status</td>
            </tr>
            @foreach ($tasks as $t)
            <tr>
                <td>{{ $t->batch }}</td>
                <td>{{ $t->filename }}</td>
                <td>{{ $t->status }} <span id="s{{$t->id}}">({{ $t->progress }}/{{ $t->totalJobs }})</span></td>
            </tr>
            <script>setInterval(refreshStatus, 1000, "{{$t->batch}}", "s{{$t->id}}");</script>
            @endforeach


        </table>
        @else
        <p>No tasks</p>
        @endif
    </div>
</div>
@endsection

@push('scripts')

@endpush