var deposittime = 0;
var withdrawtime = 0;
var intervalobjdepo = null;
var intervalobjwd = null;

function checkDeposit() {
    axios.get("/deposit/timestamp").then((result)=>{
        if(deposittime < result.data.timestamp) {
            if(deposittime == 0) {
                deposittime = result.data.timestamp;
            } else {
                deposittime = result.data.timestamp;
                document.getElementById("newdeponotif").style.display = "block";
                if(document.getElementById('nosound').checked == false) {
                    playDepoSound();
                }
            }
        }
    });
}

function checkWithdraw() {
    axios.get("/withdraw/timestamp").then((result)=>{
        if(withdrawtime < result.data.timestamp) {
            if(withdrawtime == 0) {
                withdrawtime = result.data.timestamp;
            } else {
                withdrawtime = result.data.timestamp;
                document.getElementById("newwdnotif").style.display = "block";
                if(document.getElementById('nosound').checked == false) {
                    playWDSound();
                }
            }
        }
    });
}

function playDepoSound() {
    const audio = new Audio("/sounds/deposound.mp3");
    audio.muted=false;
    audio.play();
}

function playWDSound() {
    const audio = new Audio("/sounds/wdsound.mp3");
    audio.muted=false;
    audio.play();
}

function CheckAutoplaySound() {
    const audio = new Audio("/sounds/checksound2.mp3");
    audio.play()
    .then(()=>{
        if(intervalobjdepo == null) intervalobjdepo = setInterval(checkDeposit, 5000);
        if(intervalobjwd == null) intervalobjwd = setInterval(checkWithdraw, 5000);
    })
    .catch(async ()=>{
        await Swal.fire("play without interaction is not allow. Click ok to allow or permanently set sound permission to allow.");
        if(intervalobjdepo == null) intervalobjdepo = setInterval(checkDeposit, 5000);
        if(intervalobjwd == null) intervalobjwd = setInterval(checkWithdraw, 5000);
    });
}

function startAutoCheck() {
    if(intervalobjdepo == null) intervalobjdepo = setInterval(checkDeposit, 2000);
    if(intervalobjwd == null) intervalobjwd = setInterval(checkWithdraw, 2000);
}

function setsound() {
    var chksound = document.getElementById('nosound').checked;
    if(chksound == true) {
        axios.post('/mute')
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    } else {
        axios.post('/unmute')
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    }
}
// CheckAutoplaySound();
startAutoCheck();