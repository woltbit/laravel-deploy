<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhoneBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_batches', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('totalfiles');
            $table->integer('eachno');
            $table->string('status');
            $table->string('file');
            $table->string('filevcf');
            $table->text('banned_detail')->nullable();
            $table->bigInteger('brand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_batches');
    }
}
