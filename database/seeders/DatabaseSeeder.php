<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Brand;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
        // $brandA = new Brand();
        // $brandA->name = 'Brand A';
        // $brandA->slug = 'brand-a';
        // $brandA->save();

        // $brandB = new Brand();
        // $brandB->name = 'Brand B';
        // $brandB->slug = 'brand-b';
        // $brandB->save();

    }
}
